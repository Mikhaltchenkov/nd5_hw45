'use strict';

pokemonApp.component('pokemonDetail', {
    // bindings: {
    //     pokemonId: '<'
    // },
    controller: function PokemonDetailCtrl(PokemonsService, $routeParams) {
        var ctrl = this;
        ctrl.pokemonLoaded = false;
        ctrl.notfoundError = false;
        ctrl.pokemon = PokemonsService.get({
    			'pokemonId': $routeParams['pokemonId']
    		}, function(successResult) {
    			// Окей!
    			ctrl.pokemonLoaded = true;
          ctrl.notfoundError = false;
    			ctrl.activeTab = 1;
    			ctrl.disableControlTab = true;
    		}, function(errorResult) {
    			// Не окей..
    			ctrl.notfoundError = true;
    			ctrl.pokemonLoaded = true;
    		});
        ctrl.deletePokemon = function(pokemonId) {

                ctrl.deletionError = false;
                ctrl.deletionSuccess = false;

                PokemonsService.deletePokemon(pokemonId).then(function successCallback(response) {
                    // Окей!
                    ctrl.deletionSuccess = true;
                }, function errorCallback(response) {
                    // Не окей..
                    ctrl.deletionError = true;
                });
            }
    },

    templateUrl: './src/PokemonDetail/PokemonDetail.html'

});
